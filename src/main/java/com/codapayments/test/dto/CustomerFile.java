package com.codapayments.test.dto;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CustomerFile {
    String firstName;
    String lastName;
    String companyName;
    String address;
    String city;
    String country;
    String state;
    String zip;
    String phoneNumberOne;
    String phoneNumberTwo;
    String emailAddress;
    String website;
}
