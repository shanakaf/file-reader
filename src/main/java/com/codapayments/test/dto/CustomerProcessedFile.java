package com.codapayments.test.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@ToString
@Getter
@Setter
public class CustomerProcessedFile {

    ValueType firstName;
    ValueType lastName;
    ValueType companyName;
    ValueType address;
    ValueType city;
    ValueType country;
    ValueType state;
    ValueType zip;
    ValueType phoneNumberOne;
    ValueType phoneNumberTwo;
    ValueType emailAddress;
    ValueType website;
}
