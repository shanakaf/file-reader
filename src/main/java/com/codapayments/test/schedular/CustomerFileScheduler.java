package com.codapayments.test.schedular;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static javax.batch.runtime.BatchStatus.COMPLETED;

@Service
@RequiredArgsConstructor
@Slf4j
@EnableScheduling
public class CustomerFileScheduler {

    private final JobLauncher jobLauncher;
    private final @Qualifier("customerFileJob")
    Job job;

    @Scheduled(fixedDelay = 20000L)
    public void runCustomerFileSchedule() {
        log.info("Customer File Scheduler is started");
        Map<String, JobParameter> params = new HashMap<>();
        params.put("time", new JobParameter(System.currentTimeMillis()));
        try {
            JobExecution execution = jobLauncher.run(job, new JobParameters(params));
            if (execution.getStatus().getBatchStatus() == COMPLETED) {
                log.info("Execution completed successfully");
            }
        } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
            log.error("Error in running customer file schedule ", e);
        }
    }
}
