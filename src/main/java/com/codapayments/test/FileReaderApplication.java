package com.codapayments.test;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableBatchProcessing
@RestController
public class FileReaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileReaderApplication.class, args);
    }
}
