package com.codapayments.test.job;

import com.codapayments.test.avro.CustomerFileFormat;
import com.codapayments.test.dto.CustomerFile;
import com.codapayments.test.process.CustomerFileProcessor;
import com.codapayments.test.writer.CustomerFileKafkaWriter;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

@Configuration
@RequiredArgsConstructor
public class CustomerFileProcessJob {

    @Bean("customerFileJob")
    public Job job(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory, @Qualifier("customerFileReader") FlatFileItemReader<CustomerFile> reader, CustomerFileProcessor processor, CustomerFileKafkaWriter writer) {
        Step step = stepBuilderFactory.get("fileProcessorStep")
                .<CustomerFile, CustomerFileFormat>chunk(10)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .taskExecutor(taskExecutor())
                .build();

        return jobBuilderFactory.get("fileProcessorJob")
                .incrementer(new RunIdIncrementer())
                .start(step)
                .build();
    }

    public TaskExecutor taskExecutor() {
        SimpleAsyncTaskExecutor taskExecutor = new SimpleAsyncTaskExecutor();
        taskExecutor.setConcurrencyLimit(10);
        return taskExecutor;
    }

}
