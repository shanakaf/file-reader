package com.codapayments.test.writer;

import com.codapayments.test.avro.CustomerFileFormat;
import com.codapayments.test.kafka.producer.KafkaProducerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
@RequiredArgsConstructor
public class CustomerFileKafkaWriter implements ItemWriter<CustomerFileFormat> {

    private final KafkaProducerService kafkaProducerService;

    @Value("${codapayment.topic.customer-details.name}")
    private String producerTopic;

    @Override
    public void write(List<? extends CustomerFileFormat> list) throws Exception {

        list.forEach(record -> {
            log.info("Writing customer record for user : {} to kafka ", record.getFirstName());
            kafkaProducerService.produceKafkaMessage(producerTopic, record);
        });
    }
}
