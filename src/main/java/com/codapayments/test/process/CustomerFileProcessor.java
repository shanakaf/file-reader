package com.codapayments.test.process;

import com.codapayments.test.avro.CustomerFileFormat;
import com.codapayments.test.avro.ValueType;
import com.codapayments.test.dto.CustomerFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CustomerFileProcessor implements ItemProcessor<CustomerFile, CustomerFileFormat> {
    @Override
    public CustomerFileFormat process(CustomerFile customerFile) throws Exception {
        log.info("Started converting customer details file record for user : {} ", customerFile.getFirstName());

        ValueType.Builder stringValueTypeBuilder = ValueType.newBuilder().setType("String");
        ValueType.Builder numberValueType = ValueType.newBuilder().setType("Number");
        ValueType.Builder emailValueType = ValueType.newBuilder().setType("Email address");
        ValueType.Builder phoneValueType = ValueType.newBuilder().setType("Phone number");
        ValueType.Builder webValueType = ValueType.newBuilder().setType("Web URL");

        CustomerFileFormat processedFile = CustomerFileFormat.newBuilder()
                .setFirstName(stringValueTypeBuilder.setValue(customerFile.getFirstName()).build())
                .setLastName(stringValueTypeBuilder.setValue(customerFile.getLastName()).build())
                .setCompanyName(stringValueTypeBuilder.setValue(customerFile.getCompanyName()).build())
                .setAddress(stringValueTypeBuilder.setValue(customerFile.getAddress()).build())
                .setCity(stringValueTypeBuilder.setValue(customerFile.getCity()).build())
                .setCountry(stringValueTypeBuilder.setValue(customerFile.getCompanyName()).build())
                .setState(stringValueTypeBuilder.setValue(customerFile.getState()).build())
                .setZip(numberValueType.setValue(customerFile.getZip()).build())
                .setPhoneNumberOne(phoneValueType.setValue(customerFile.getPhoneNumberOne()).build())
                .setPhoneNumberTwo(phoneValueType.setValue(customerFile.getPhoneNumberTwo()).build())
                .setEmailAddress(emailValueType.setValue(customerFile.getEmailAddress()).build())
                .setWebsite(webValueType.setValue(customerFile.getWebsite()).build())
                .build();
        log.info("End converting customer details file record for user : {} ", customerFile.getFirstName());
        return processedFile;
    }
}
