package com.codapayments.test.config;

import com.codapayments.test.dto.CustomerFile;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class FileLineMapper {

    @Bean(name = "customerFileLineMapper")
    @Primary
    public LineMapper<CustomerFile> customerFileLineMapper() {
        DefaultLineMapper<CustomerFile> lineMapper = new DefaultLineMapper<>();

        BeanWrapperFieldSetMapper<CustomerFile> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(CustomerFile.class);
        lineMapper.setFieldSetMapper(fieldSetMapper);

        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames("firstName", "lastName", "companyName", "address", "city", "country", "state", "zip", "phoneNumberOne", "phoneNumberTwo", "emailAddress", "website");
        lineMapper.setLineTokenizer(lineTokenizer);
        return lineMapper;
    }
}
