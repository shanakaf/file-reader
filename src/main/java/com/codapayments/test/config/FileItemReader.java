package com.codapayments.test.config;

import com.codapayments.test.dto.CustomerFile;
import com.codapayments.test.loader.FileLoader;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class FileItemReader {

    private final FileLoader fileLoader;

    @Bean(name = "customerFileReader")
    @StepScope
    public FlatFileItemReader<CustomerFile> customerFlatFileItemReader(@Qualifier("customerFileLineMapper") LineMapper<CustomerFile> lineMapper) {
        FlatFileItemReader<CustomerFile> reader = new FlatFileItemReader<>();
        reader.setResource(fileLoader.loadFile());
        reader.setLinesToSkip(1);
        reader.setLineMapper(lineMapper);
        return reader;
    }
}
