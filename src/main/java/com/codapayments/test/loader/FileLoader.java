package com.codapayments.test.loader;

import org.springframework.core.io.Resource;

public interface FileLoader {

    Resource loadFile();
}
