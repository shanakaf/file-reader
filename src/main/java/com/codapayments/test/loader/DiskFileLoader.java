package com.codapayments.test.loader;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;


@Component
@ConditionalOnProperty(name = "codapayment.batch-file.source", havingValue = "disk")
@Slf4j
public class DiskFileLoader implements FileLoader {

    @Value("${codapayment.batch-file.source.location}")
    private String filePath;

    @Override
    public Resource loadFile() {
        log.info("Loading file from {} ", filePath);
        Resource resource = new FileSystemResource(filePath);
        return resource;
    }
}
