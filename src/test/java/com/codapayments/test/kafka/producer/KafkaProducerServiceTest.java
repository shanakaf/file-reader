package com.codapayments.test.kafka.producer;

import com.codapayments.test.avro.CustomerFileFormat;
import com.codapayments.test.avro.ValueType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.Message;
import org.springframework.util.concurrent.ListenableFuture;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class KafkaProducerServiceTest {

    private KafkaProducerService producer;

    @Mock
    private KafkaTemplate kafkaTemplate;

    @Captor
    private ArgumentCaptor<Message<?>> messageArgumentCaptor;

    private CustomerFileFormat message;

    @Before
    public void setUp() {
        producer = new KafkaProducerService(kafkaTemplate);

        ValueType valueType = ValueType.newBuilder()
                .setType("String")
                .setValue("Test")
                .build();

        message = CustomerFileFormat.newBuilder()
                .setFirstName(valueType)
                .setLastName(valueType)
                .setAddress(valueType)
                .setCity(valueType)
                .setCompanyName(valueType)
                .setCountry(valueType)
                .setState(valueType)
                .setZip(valueType)
                .setEmailAddress(valueType)
                .setPhoneNumberOne(valueType)
                .setPhoneNumberTwo(valueType)
                .setWebsite(valueType)
                .build();
    }

    @Test
    public void givenValidMessage_WhenSend_ThenProduceSuccessfully() {
        ListenableFuture future = Mockito.mock(ListenableFuture.class);
        when(kafkaTemplate.send(any(Message.class))).thenReturn(future);
        producer.produceKafkaMessage("topic-name", message);
        verify(kafkaTemplate).send(messageArgumentCaptor.capture());
        assertEquals(message, messageArgumentCaptor.getValue().getPayload());
    }
}