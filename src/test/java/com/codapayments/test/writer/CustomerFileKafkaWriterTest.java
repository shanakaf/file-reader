package com.codapayments.test.writer;

import com.codapayments.test.avro.CustomerFileFormat;
import com.codapayments.test.kafka.producer.KafkaProducerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.newArrayList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles("test")
public class CustomerFileKafkaWriterTest {

    private CustomerFileKafkaWriter kafkaWriter;

    @Mock
    private KafkaProducerService producerService;

    @Captor
    private ArgumentCaptor<CustomerFileFormat> messageCaptor;

    @Value("${codapayment.topic.customer-details.name}")
    private String producerTopic;

    @Before
    public void setUp() {
        kafkaWriter = new CustomerFileKafkaWriter(producerService);
    }

    @Test
    public void givenSetOfMessage_WhenWrite_ThenProduceViaKafka() throws Exception {
        List<CustomerFileFormat> messages = newArrayList(
                new CustomerFileFormat(),
                new CustomerFileFormat()
        );
        kafkaWriter.write(messages);

        InOrder inOrder = inOrder(producerService);
        inOrder.verify(producerService, times(2)).produceKafkaMessage(eq(producerTopic), messageCaptor.capture());
        assertThat(messageCaptor.getAllValues()).hasSize(2);
    }
}