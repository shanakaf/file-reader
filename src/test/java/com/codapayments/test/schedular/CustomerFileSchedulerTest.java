package com.codapayments.test.schedular;

import org.junit.jupiter.api.Test;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;

@SpringJUnitConfig(CustomerFileScheduler.class)
public class CustomerFileSchedulerTest {

    @SpyBean
    private CustomerFileScheduler scheduler;

    @MockBean
    private JobLauncher jobLauncher;

    @MockBean
    private @Qualifier("customerFileJob")
    Job job;

    @Test
    public void givenSpecificTime_WhenLaunch_CheckSchedulerRunSuccessfully() {
        await().atMost(50, TimeUnit.SECONDS)
                .untilAsserted(() -> verify(scheduler, atLeast(2)).runCustomerFileSchedule());
    }


}